// NODE JS INTRODUCTION

// use the "require" directive to load Node.js modules.
// a "module" is a software component or part of a program that contains one or more routines.
// The "http module" lets node.js transfer data using the hyper text tranfer protocol.
// "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications.
// HTTP is a protocol that allow the fetching of resources such as HTML documents.
// clients(browsers) and servers (node.js/express.js applications) communicate by exchanging individual messages.
// the messages sent by the client, usually, a web browser, are called request.
// the messages sent by the server as an answer are called reponses.

let http = require("http");

// the http module has a createServer method that accepts a function as an argument and allows for a creation of a server.
// the arguments passed in the fuction are request and response objects that contains methods that allow us to receive request from the client and send responses back to it.
http.createServer(function (request, response) {
// use the writeHead() method to:
// set a status code for the reponse - a 200 means OK
// set the Content-Type of the response as a plain text message.
	response.writeHead(200, {'Content-Type': 'text/plain'})
// send the response with a text content "Hello World".
	response.end('Hello World');
}).listen(4000)

// a port is a virtual point where network connection start and end
// each port is associated with a specific process or service
// the server will be assignwd to port 4000 via the ".listen(4000)" method where the server


console.log('Server running at localhost:4000');